import './App.css';
import {counterHOC} from './CounterHOC';


const AppHocDemo = ({ count, onDecrease, onIncrease }) => {
  console.log(count);
  return (
    <div className='text-center'>
      <div>Current count: {count}</div>
      <div>
        <button onClick={onDecrease}>-</button>
        <button onClick={onIncrease}>+</button>
      </div>
    </div>
  );
}

const FinalHOCDemo = counterHOC(AppHocDemo);

export default FinalHOCDemo;