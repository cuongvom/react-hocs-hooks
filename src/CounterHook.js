import { useState } from "react"

const UseCounter = () => {
    const [count, setCount] = useState(0);

    const inCrease = () => setCount(count + 1);
    const deCrease = () => setCount(count - 1);
    return [count, deCrease, inCrease];
}

export default UseCounter;
