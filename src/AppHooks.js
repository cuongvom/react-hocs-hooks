import { useEffect, useRef, useState } from "react";
import UseCounter from "./CounterHook";

const FinalHooksDemo = () => {
    const [count, deCrease, inCrease] = UseCounter();
    const [status, setStatus] = useState(true);
    console.log(count);
    useEffect(() => {
        console.log(`effected ${status}`);
    }, [status]);

    return (
        <div className="tex-center">
            <div>Current count: {count}</div>
            <div>Before count: {usePrevious(count)}</div>
            <div>
                <button onClick={deCrease}>-</button>
                <button onClick={inCrease}>+</button>
            </div>
            <button onClick={() => { if (status) setStatus(false); else setStatus(true); }}>Status</button>
        </div>
    );
}
const usePrevious = value => {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

export default FinalHooksDemo;