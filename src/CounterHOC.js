import React from "react";

export const counterHOC = WrappedComponent => {
    return class CounterHOC extends React.Component {
        state = {
            count: 0
        }

        handleDecrement = () => {
            this.setState({ count: this.state.count - 1 });
        };

        handleIncrement = () => {
            this.setState({ count: this.state.count + 1 });
        };

        render() {
            const wrappedComponentDisplayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';
            console.log(wrappedComponentDisplayName);
            const { count } = this.state;

            return (
                <WrappedComponent {...this.prop} count={count} onIncrease={this.handleIncrement} onDecrease={this.handleDecrement} />
            );
        }
    };
};